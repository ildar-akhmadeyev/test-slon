export default {
  setPopup: (state, popupName) => (state.popup = popupName),
  setTrends: (state, items) => (state.trends = items),
};
