import trends from './trends.json';

export default {
  async loadData(store) {
    await setTimeout(() => {}, 500);
    store.commit('setTrends', trends);
  },
};
